vim.g.markdown_fenced_languages = {
	"html",
	"javascript",
	"js=javascript",
	"json",
	"go",
	"python",
	"sh",
	"bash=sh",
	"sql",
	"typescript",
	"ts=typescript",
	"yaml",
}
